import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native'



function Header ({modalVisible, score}){
const [counter, setCounter] = useState(3)


useEffect(()=>{ 
  
  const timeout = setTimeout(() => {
    if(counter>0){
      let i=counter-1
    setCounter(i);
    }
  }, 1000);


  if(counter==0){
    modalVisible(true)
  }
  return () => clearTimeout(timeout);

}, [counter])

useEffect(()=>{
  setCounter(3)
}, [score])


  
      return (
    <View style={styles.header}>
      <Text style={styles.second}>
        {counter}
      </Text>
    </View>
  
 
      );
    }
  
  
  const styles = StyleSheet.create({

    second: {
      color: 'red',
      fontSize: 32
    }
  });

export{
    Header
}