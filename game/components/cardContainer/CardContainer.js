import React  from 'react';
import {View, ImageBackground, StyleSheet} from 'react-native'
import{Card} from '../card/Card'

function CardContainer({cards, handleChooseCard}){
    return(
        <View style={styles.wrapperCard}>
            {cards.map((item)=> {
            return  (
        <Card 
            src={item.src} 
            key={item.id}
            idKey={item.id}
            chooseCard={handleChooseCard}
        />)
    })}
        </View>
    )
}
const styles = StyleSheet.create({
    wrapperCard: {
        width: '100%',
        flexDirection: 'row',
        flexWrap:'wrap',
    }
})

export{
    CardContainer
}