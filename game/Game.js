import React, {useState, useEffect} from 'react';
import {View, ImageBackground, StyleSheet, Text} from 'react-native'
import {StartScreen} from './screen/start/start'
import {PlayScreen} from './screen/play/playScreen'
import {ModalScreen} from './screen/gameOver/Modal'

const cards= [
    {
      src: require('./assets/card1.png'),
      id: 0,
      }, 
    {
      src: require('./assets/card2.png'),
      id: 1,
    },
    {
      src: require('./assets/card3.png'),
      id: 2,
    },
    {
      src: require('./assets/card4.png'),
       id: 3,
    }
]


function Game() {
const [isPlay, setIsPlay] = useState(false)
const [cardsState, setCardsState] = useState(cards);
const [findCard, setFindCard] = useState(randomIndex(cards));
const [modalVisible, setModalVisible] = useState(false);
const [score, setScore] = useState(0);
const [bestScore, setBestScore] = useState(0)
const [runAnimation, setRunAnimation] = useState(true)

function shuffleArray (arr) {
    let newArr = arr.sort(() => Math.random() - 0.5)
    return newArr;
};

function randomIndex(arr){
    const index = Math.floor(Math.random() * arr.length)
    return arr[index];
}

function handleChooseCard (id) {
    let newArr = [];
    console.log("FIND ID" + findCard.id)
    cardsState.forEach((item, index) => {
      newArr[index] = item;
      if( id === newArr[index].id){
        if (id === findCard.id  ){
          let value = score + 1;
          setScore(value)  
          console.log("SCORE " +score)
          console.log("BESTSCORE "+bestScore)
          setRunAnimation(false)
         
        } else if(id !== findCard.id ){
          setTimeout(()=>{
            setModalVisible(true)
          }, 1000)
        }
      }
  
    });
    setCardsState(newArr)
    return newArr
  }

  useEffect(()=>{
    setRunAnimation(true)
    if(score > bestScore){
        setBestScore(score)
        console.log('Change Score ' +score)
        console.log('Change bestScore ' +bestScore)
    }
   setFindCard(randomIndex(cards))
   setCardsState(shuffleArray(cards))
    
  }, [ score, modalVisible])


    return(
        <ImageBackground 
            source={require('./assets/BG.png')}
            style={styles.imageBackground}
        >
        <ModalScreen 
            value={modalVisible}
            press={()=>setModalVisible(false)}
            newScore={score}
            bestScoreValue={bestScore}
            resetScore={()=>setScore(0)}
        />

        {isPlay? 
            (<PlayScreen 
                data={cardsState}
                chooseCard={findCard}
                handleChoose={handleChooseCard}
                scoreValue={score}
                handleModal={setModalVisible}
                run={runAnimation}
            />)
            :
           ( <StartScreen 
                handleStart={()=>setIsPlay(true)}
                
            />)
        } 


        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    imageBackground:{
        height: '100%',
        width: '100%',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        color: '#ffff'
    }

})


export{
    Game
}