import React, {useState} from 'react';
import {View, Image, StyleSheet, Text} from 'react-native'
import {Header} from '../../components/header/Header'
import {CardContainer} from '../../components/cardContainer/CardContainer'

function PlayScreen ({data, chooseCard,  handleChoose,  scoreValue, handleModal, run}){
    return(
        <View style={styles.playContainer}>
           <Header score={scoreValue} modalVisible={handleModal} runAnimation={run}/>
            <Image source={chooseCard.src} 
                style={styles.img}
            /> 
            <CardContainer 
                cards={data}
                handleChooseCard={handleChoose}
            />
            <Text style={styles.score}>{scoreValue}</Text>
        </View>
     
    )
}

const styles = StyleSheet.create({
    playContainer: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    img: {
        width: 230,
        height: 230
    },
    score: {
        color: '#fff',
        fontSize: 30,
        fontWeight:'bold'
    }
})

export {
    PlayScreen
}