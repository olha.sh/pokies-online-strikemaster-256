import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Modal,
  TouchableOpacity,
} from 'react-native';


function ModalScreen ({value, press, newScore, resetScore, bestScoreValue}){
function replayGame(){
    press()
    resetScore()
    console.log("newScore "+newScore)
}

    return(
    <Modal
        transparent={true}
        visible={value}
    >
        <ImageBackground 
        source={require('../../assets/modalBg.png')}
        style={styles.modalBg}
        >
            <View style={styles.modalBox}>
                <Text style={styles.modalText}>Game over</Text>
                <Text style={styles.modalTxt1}> New {newScore} </Text>
                <Text style={styles.modalTxt2}> Best {bestScoreValue} </Text>
                <TouchableOpacity onPress={replayGame}>
                    <ImageBackground 
                        resizeMode='contain'
                        source={require('../../assets/btnRep.png')}
                        style={styles.btnRep}
                    >
                       <Text style={styles.btnText}>Replay</Text> 
                    </ImageBackground>
                    
                </TouchableOpacity>
            </View>
        </ImageBackground>
    </Modal>
      
    )
}

export{
    ModalScreen
}


const styles = StyleSheet.create({
    modalBg: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalBox:{
        width: 250,
        height: 250,
        borderRadius: 40,
        borderWidth: 4,
        borderColor: "#fff",

        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTxt1: {
        color: '#fff',
        textTransform: 'uppercase',
        fontSize: 30,
        fontWeight: 'bold'
    },
    modalTxt2: {
        color: '#fff',
        textTransform: 'uppercase',
        fontSize: 28,
    },
    modalText:{
        color: '#da2e2e',
        textTransform: 'uppercase',
        fontSize: 32,
        fontWeight: 'bold'
    },
    btnRep:{
        width: 180,
        height: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnText: {
        color: '#fff',
        textTransform: 'uppercase',
        fontSize: 30,
        fontWeight: 'bold'
    }

})